import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SlideCameraPageRoutingModule } from './slide-camera-routing.module';

import { SlideCameraPage } from './slide-camera.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SlideCameraPageRoutingModule
  ],
  declarations: [SlideCameraPage]
})
export class SlideCameraPageModule {}
