import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SlideCameraPage } from './slide-camera.page';

const routes: Routes = [
  {
    path: '',
    component: SlideCameraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SlideCameraPageRoutingModule {}
