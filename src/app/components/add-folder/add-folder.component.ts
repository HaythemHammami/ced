import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HelperService } from 'src/app/helpers/helper.service';

@Component({
  selector: 'app-add-folder',
  templateUrl: './add-folder.component.html',
  styleUrls: ['./add-folder.component.scss'],
})
export class AddFolderComponent implements OnInit {
number : string = "";
name : string = ""; 
  constructor(    private modalCtrl: ModalController,public helper:HelperService
    ) { }

  ngOnInit() {}

  dismissModal() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }

  addFolder() {
    var Folder = {
      number : this.number,
      name : this.name,
      pictures:[]
    }
    var checkFolder  = this.helper.ListFolder.filter(function(element){ return element.number == Folder.number; })
    console.log(checkFolder)
    if(checkFolder.length > 0){
      this.helper.fullAlert("file exist with this number")
    } else {
      this.helper.addFolder(Folder);
      this.dismissModal();
    }
  }
}
