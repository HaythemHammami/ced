import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; //CHANGE(ADD)
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { AddFolderComponent } from '../components/add-folder/add-folder.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [HomePage,AddFolderComponent]
})
export class HomePageModule {}
