import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SlideCameraPage } from './slide-camera.page';

describe('SlideCameraPage', () => {
  let component: SlideCameraPage;
  let fixture: ComponentFixture<SlideCameraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideCameraPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SlideCameraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
