import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Folder } from '../model/folder';

@Injectable({
  providedIn: 'root'
})

export class HelperService {

  ListFolder : Array<Folder> = []

  constructor(public alertController: AlertController) { }

  intilazeListFolder(){
    var db = JSON.parse(localStorage.getItem('Folders'));
    if(db == null){
      localStorage.setItem('Folders', JSON.stringify(this.ListFolder));
    } else {
      this.ListFolder = db;
    }
  }

  addFolder(folder){
    this.ListFolder.push(folder);
    localStorage.setItem('Folders', JSON.stringify(this.ListFolder));
  }

  addPic(photo,id){
     var selectedFolder  = this.ListFolder.filter(function(element){ return element.number == id; })
     selectedFolder[0].pictures.push({img:photo,date:new Date()});
     try {
          localStorage.setItem('Folders', JSON.stringify(this.ListFolder));
          console.log("LocalStorageIsNOTFull");
      }
      catch (e) {
        this.fullAlert('there is no space your image will be showed but not saved .');
          console.log("Local Storage is full, Please empty data");
          console.log(e);
      }
  }

  async fullAlert(msg) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: ['OK']
    });
    await alert.present();
  }
  
  getPic(id ){
    var selectedFolder  = this.ListFolder.filter(function(element){ return element.number == id; })
    return selectedFolder[0].pictures;
  }
}
