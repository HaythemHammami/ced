import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddFolderComponent } from '../components/add-folder/add-folder.component';
import { HelperService } from '../helpers/helper.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  constructor(public helper  : HelperService,public modalController: ModalController) {
  }
  ngOnInit(){
  }
  
  async presentModal() {
    const modal = await this.modalController.create({
      component: AddFolderComponent,
    });
    return await modal.present();
  }
}
