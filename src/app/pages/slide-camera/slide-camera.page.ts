import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { HelperService } from 'src/app/helpers/helper.service';

@Component({
  selector: 'app-slide-camera',
  templateUrl: './slide-camera.page.html',
  styleUrls: ['./slide-camera.page.scss'],
})
export class SlideCameraPage implements OnInit {

  constructor(private route: ActivatedRoute,private sanitizer: DomSanitizer,public helper : HelperService) { }
  
  photos : any = []; 
  id : string ;

  ngOnInit() {
     this.id = this.route.snapshot.paramMap.get('id');
     this.photos = this.helper.getPic(this.id);
  }

  transform(img){
    return this.sanitizer.bypassSecurityTrustResourceUrl(img);
  }
  
  async takePicture() {

    const image = await Plugins.Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.helper.addPic(image.dataUrl,this.id)
  }

}
